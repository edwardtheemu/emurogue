﻿namespace EmuRogue.Settings
{
    public class EmuRogueSettings
    {
        public volatile bool UseKick = true;
        public volatile int UseAdrenalineRush = 2;
        public volatile int UseBladeFlurry = 2;
        public volatile int UseBloodFury = 2;
        public volatile int UseEvasion = 2;
        public volatile bool UseSprint = true;
        public volatile int MinimumSprintDistance = 15;
        public volatile bool AlwaysStealthed = false;
        public volatile bool UseRangedToPull = false;
        public volatile string RangedPullSpell = "Throw";
        public volatile string RangedAmmo = "Small Throwing Knife";
        public volatile int RangedAttackRange = 25;
        public volatile int MeleeAttackRange = 4;

        public string FoodName = "";


        private EmuRogueSettings()
        {

        }

        internal static EmuRogueSettings Values { get; set; } = new EmuRogueSettings();

        internal void Load()
        {
            Values = ZzukBot.Settings.OptionManager.Get("EmuRogueSettings").LoadFromJson<EmuRogueSettings>() ??
                     this;
        }

        internal void Save()
        {
            ZzukBot.Settings.OptionManager.Get("EmuRogueSettings").SaveToJson(this);
        }

    }

}
