﻿using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Drawing.Text;
using System.Linq;
using EmuRogue.Constants;
using ZzukBot.ExtensionFramework.Classes;
using ZzukBot.Constants;
using EmuRogue.Settings;
using System.Linq.Expressions;
using System.Windows.Forms;
using EmuRogue.Gui;
using ZzukBot.ExtensionFramework;
using ZzukBot.Game.Statics;
using ZzukBot.Mem;

namespace EmuRogue
{
    [Export(typeof(CustomClass))]
    public class EmuRogue : CustomClass
    {
        private Form RogueGui = new EmuRogueMain();

        /// <summary>
        /// The WoW class the CustomClass is designed for
        /// </summary>
        public override Enums.ClassId Class => Enums.ClassId.Rogue;

        //Check if our lua functions are registered
        //If not then register them
        private void LoadFunctions()
        {
            if (!Lua.Instance.GetText("", Data.CheckFunctionsLoaded).Contains("1"))
            {
                Lua.Instance.Execute(Data.RegisterGetBuffDuration);
                Lua.Instance.Execute(Data.RegisterGetSpellRank);
                Lua.Instance.Execute(Data.RegisterGetSpellId);
                Lua.Instance.Execute(Data.RegisterFunctionsLoaded);
                Lua.Instance.Execute(Data.PrintToChat("Registered Lua Functions."));
            }
        }

        private bool ShouldWeEviscerate(int parTargetHealth, int parComboPoints, int maxHealth = 90)
        {
            //Some servers only give the percentage value
            if (maxHealth.Equals(100))
            {
                return parComboPoints == 5;
            }

            string rankResult = Lua.Instance.GetText("emuRank = getSpellRank('Eviscerate')", "emuRank");
            int rank = Convert.ToInt32(rankResult.Substring(rankResult.Length-2, 2));
            if (rank < 1 || parComboPoints < 1) return false;
            int damage = (Data.EviscerateDamage[rank][parComboPoints] + Data.EviscerateDamage[rank][0]);
            //Lua.Instance.Execute(Data.PrintToChat("Rank: " + rank + " || Est Dmg: " + damage + "|| Enemy Hp: " + parTargetHealth ));
            return parTargetHealth <= damage || parComboPoints == 5;
        }

        private double GetSliceAndDiceDuration()
        {
            try
            {
                return Convert.ToDouble(Lua.Instance.GetText("points = getBuffDuration('" + Data.SliceAndDiceTexture + "')", "points"));

            }
            catch
            {
                return -3;
            }
        }

        private void TryCast(string parSpell, int parWait = 0)
        {
            if (CanCast(parSpell))
            {
                Spell.Instance.CastWait(parSpell, parWait);
            }
        }

        private bool CanCast(string parSpell)
        {
            return Spell.Instance.IsSpellReady(parSpell) && Spell.Instance.GetSpellRank(parSpell) != 0;
        }

        /// <summary>
        /// Should be called when the CC is loaded
        /// </summary>
        /// <returns></returns>
        public override bool Load()
        {
            //Make sure our Json settings are loaded
            EmuRogueSettings.Values.Load();
            LoadFunctions();
            return true;
        }

        /// <summary>
        /// Should be called when the CC is unloaded
        /// </summary>
        public override void Unload()
        {
        }

        /// <summary>
        /// Should be called when the botbase is pulling an unit
        /// </summary>
        public override void OnPull()
        {
            //We are using minimal lua / mainthread functions - no need to invoke

            var targetUnit = ObjectManager.Instance.Target;
            
            if (targetUnit == null)
                return;


            //Are we using ranged to pull?
            if (EmuRogueSettings.Values.UseRangedToPull && 
                (Inventory.Instance.GetEquippedItem(Enums.EquipSlot.Ranged).Name == EmuRogueSettings.Values.RangedAmmo ||
                Inventory.Instance.GetEquippedItem(0).Name == EmuRogueSettings.Values.RangedAmmo))
            {
                CustomClasses.Instance.Current.CombatDistance = EmuRogueSettings.Values.RangedAttackRange;
                TryCast(EmuRogueSettings.Values.RangedPullSpell, 3000);
                return;
            }
            else
            {
                CustomClasses.Instance.Current.CombatDistance = EmuRogueSettings.Values.MeleeAttackRange;

                if (EmuRogueSettings.Values.UseSprint &&
                    targetUnit.DistanceToPlayer > EmuRogueSettings.Values.MinimumSprintDistance)
                {
                    TryCast("Sprint");
                }
                //Stealth Opener
                if (ObjectManager.Instance.Player.GotAura("Stealth"))
                {
                    if (CanCast("Cheap Shot"))
                    {
                        //Prevent auto-attacking out of stealth
                        Spell.Instance.StopAttack();
                        TryCast("Cheap Shot");
                        return;
                    }
                    else
                    {
                        TryCast("Sinister Strike");
                    }
                }
                else //Regular Opener
                {
                    TryCast("Sinister Strike");
                }
                //Attack if arent trying to cheapshot / use ranged right now!
                Spell.Instance.Attack();
            }
        }

        /// <summary>
        /// Should be called when the botbase is fighting an unit
        /// </summary>
        public override void OnFight()
        {
            //We are using alot of Lua - Faster to execute everything in the mainthread
            MainThread.Instance.Invoke(() =>
            {
                //Copy objects & variables at current state to prevent null object exceptions
                var numAttackers = UnitInfo.Instance.NpcAttackers.Count;
                var targetUnit = ObjectManager.Instance.Target;
                var energy = ObjectManager.Instance.Player.Energy;
                var comboPoints = ObjectManager.Instance.Player.ComboPoints;


                if (targetUnit == null)
                    return;

                //Make sure our functions are loaded
                LoadFunctions();

                //Start AutoAttack
                Spell.Instance.Attack();

                /*If we are under attack from multiple mobs*/
                if (EmuRogueSettings.Values.UseAdrenalineRush > 0)
                {
                    if (numAttackers >= EmuRogueSettings.Values.UseAdrenalineRush)
                        TryCast("Adrenaline Rush");
                }

                if (EmuRogueSettings.Values.UseBloodFury > 0)
                {
                    if (numAttackers >= EmuRogueSettings.Values.UseBloodFury)
                        TryCast("Blood Fury");
                }

                if (EmuRogueSettings.Values.UseBladeFlurry > 0)
                {
                    if (numAttackers >= EmuRogueSettings.Values.UseBladeFlurry)
                        TryCast("Blade Flurry");
                }

                if (EmuRogueSettings.Values.UseEvasion > 0)
                {
                    if (numAttackers >= EmuRogueSettings.Values.UseEvasion)
                        TryCast("Evasion");
                }



                //We can't do anything!
                if (energy <= 20)
                {
                    return;
                }

                //Kick Logic
                if (EmuRogueSettings.Values.UseKick && energy >= 25 &&
                    (targetUnit.Casting != 0 || targetUnit.Casting != 0))
                {
                    TryCast("Kick");
                }

                if (energy >= 35)
                {
                    //Check if we have 5 combo points or Eviscerate should kill the target
                    if (ShouldWeEviscerate(targetUnit.Health, comboPoints, targetUnit.MaxHealth))
                    {
                        TryCast("Eviscerate");
                        return;
                    }

                }

                if (energy >= 25)
                {
                    if (CanCast("Slice and Dice"))
                    {
                        //If we don't have slice and dice up
                        //or if slice and dice has less than 2.0 secs left
                        //BUT! If next eviscerate will kill the target wait for energy instead
                        if ((!ObjectManager.Instance.Player.GotAura("Slice and Dice") ||
                             this.GetSliceAndDiceDuration() <= 2.0) &&
                            !this.ShouldWeEviscerate(targetUnit.Health, comboPoints, targetUnit.MaxHealth) && comboPoints > 0)
                        {
                            TryCast("Slice and Dice");
                            return;
                        }
                    }
                    //If nothing else, cast SS
                    if (energy >= 40)
                    {
                        TryCast("Sinister Strike");
                    }

                }
            });
        }

        /// <summary>
        /// Should be called when the botbase is resting
        /// </summary>
        public override void OnRest()
        {
            //This needs to be tested & cleaned up
            MainThread.Instance.Invoke(() =>
            {
                try
                {
                    if (!ObjectManager.Instance.Player.IsEating)
                    {
                        ObjectManager.Instance.Items.FirstOrDefault(i => i.Name == EmuRogueSettings.Values.FoodName)
                            .Use();
                        ZzukBot.Helpers.Wait.For("EatRogue", 500);
                    }
                }
                catch
                {

                }
            });
            if(EmuRogueSettings.Values.AlwaysStealthed)
                TryCast("Stealth");
        }

        /// <summary>
        /// Should be called when the botbase is resting
        /// </summary>
        /// <returns>
        /// Returns true when the character is done buffing
        /// </returns>
        public override bool OnBuff()

        {
            if (ObjectManager.Instance.Player.IsMainhandEnchanted() && ObjectManager.Instance.Player.IsOffhandEnchanted())
                return true;

            if (ObjectManager.Instance.Player.CastingAsName != "")
                return false;

            
            //Retrieve the most powerful posion in our inventory
            if (Inventory.Instance.GetLastItem(Data.Posions) != "")
            {
                if (!ObjectManager.Instance.Player.IsMainhandEnchanted())
                {
                    ObjectManager.Instance.Player.EnchantMainhandItem(Inventory.Instance.GetLastItem(Data.Posions));
                    return false;
                }

                if (!ObjectManager.Instance.Player.IsOffhandEnchanted())
                {
                    ObjectManager.Instance.Player.EnchantOffhandItem(Inventory.Instance.GetLastItem(Data.Posions));
                    return false;
                }
            }

            //If we want to stay stealthed and there are no lootable mobs 
            if (EmuRogueSettings.Values.AlwaysStealthed && ObjectManager.Instance.Npcs.Count(i => i.CanBeLooted) == 0)
                TryCast("Stealth");

            return true;
        }

        /// <summary>
        /// Should be called to show the settings form
        /// </summary>
        public override void ShowGui()
        {
            RogueGui.Dispose();
            RogueGui = new EmuRogueMain();
            RogueGui.Show();
        }

        /// <summary>
        /// The name of the CC
        /// </summary>
        public override string Name => "EmuRogue";

        /// <summary>
        /// The author of the CC
        /// </summary>
        public override string Author => "Emu";

        /// <summary>
        /// The version of the CC
        /// </summary>
        public override int Version => 1;

        /// <summary>
        /// The current combat distance
        /// </summary>
        public override float CombatDistance => 4;

    }
}
