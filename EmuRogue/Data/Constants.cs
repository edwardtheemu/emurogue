﻿namespace EmuRogue.Constants
{
    internal static class Data
    {
        //Useful Lua Stuff
        public static string PrintToChat(string parMessage)
        {
            return "DEFAULT_CHAT_FRAME:AddMessage('EmuRogue: " + parMessage + "')";
        }

        public static string SliceAndDiceTexture = @"Interface\\Icons\\Ability_Rogue_SliceDice";

        public static string RegisterFunctionsLoaded = "EmuRogueFunctionsLoaded = 1";
        public static string CheckFunctionsLoaded = "EmuRogueFunctionsLoaded";

        public static string RegisterGetBuffDuration = "function getBuffDuration(name) GetSpellForBot = name " +
                                                       "timeleft = -1 for i=0,31 do local id,cancel = GetPlayerBuff(i,'HELPFUL|HARMFUL|PASSIVE')" +
                                                       " if(name == GetPlayerBuffTexture(id)) then " +
                                                       "timeleft = GetPlayerBuffTimeLeft(id) " +
                                                       " return timeleft end end return timeleft end";


        public static string RegisterGetSpellRank = "function getSpellRank(SpellName) if(1) then" +
                                                    " llName, spellRank = GetSpellName(getSpellId(SpellName), BOOKTYPE_SPELL) " +
                                                    "return spellRank end end";

        public static string RegisterGetSpellId = "function getSpellId(SpellName) " +
                                                  "local B = 'BOOKTYPE_SPELL' " +
                                                  "local SpellRank = 0 " +
                                                  "local SpellID = nil " +
                                                  "if SpellName then " +
                                                  "local SpellCount = 0 " +
                                                  "local ReturnName = nil " +
                                                  "local ReturnRank = nil " +
                                                  "while SpellName ~= ReturnName do " +
                                                  "SpellCount = SpellCount + 1 " +
                                                  "ReturnName, ReturnRank = GetSpellName(SpellCount, B) " +
                                                  "if not ReturnName then " +
                                                  "break " +
                                                  "end " +
                                                  "end " +
                                                  "while SpellName == ReturnName do " +
                                                  "if SpellRank then " +
                                                  "if SpellRank == 0 then " +
                                                  "return SpellCount " +
                                                  "elseif ReturnRank and ReturnRank ~= '' then " +
                                                  "local found, _, Rank = string.find(ReturnRank, '(%d+)') " +
                                                  "if found then " +
                                                  "ReturnRank = tonumber(Rank) " +
                                                  "else " +
                                                  "ReturnRank = 1 " +
                                                  "end " +
                                                  "else " +
                                                  "ReturnRank = 1 " +
                                                  "end " +
                                                  "if SpellRank == ReturnRank then " +
                                                  "return SpellCount " +
                                                  "end " +
                                                  "else " +
                                                  "SpellID = SpellCount " +
                                                  "end " +
                                                  "SpellCount = SpellCount + 1 " +
                                                  "ReturnName, ReturnRank = GetSpellName(SpellCount, B) " +
                                                  "end " +
                                                  "end " +
                                                  "return SpellId " +
                                                  "end ";

        //Other Stuff

        public static string[] Posions =
        {
            "Instant Poison", "Instant Poison", "Instant Poison II", "Instant Poison III",
            "Instant Poison IV", "Instant Poison V", "Instant Poison VI"
        };

        public static int[][] EviscerateDamage = new int[][]
        {
            //Element at 0 is estimated white swing dmg
            new int[] {0, 0, 0, 0, 0},
            new int[] {10, 10, 15, 20, 25, 30},
            new int[] {20, 22, 33, 44, 55, 66},
            new int[] {30, 39, 58, 77, 96, 115},
            new int[] {50, 61, 92, 123, 154, 185},
            new int[] {60, 60, 135, 180, 225, 270},
            new int[] {80, 143, 220, 297, 374, 451},
            new int[] {100, 212, 322, 432, 542, 652},
            new int[] {150, 295, 446, 597, 748, 899},
            new int[] {200, 332, 502, 672, 842, 1012},
        };
    }
}
