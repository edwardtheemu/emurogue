﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EmuRogue.Settings;

namespace EmuRogue.Gui
{
    public partial class EmuRogueMain : Form
    {
        public EmuRogueMain()
        {
            InitializeComponent();
        }

        private void adrLbl_Click(object sender, EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            //Ranged
            EmuRogueSettings.Values.UseRangedToPull = useRangedCb.Checked;
            EmuRogueSettings.Values.RangedPullSpell = rangedSpellTb.Text;
            EmuRogueSettings.Values.RangedAmmo = rangedAmmoTb.Text;

            //Damage Cooldowns
            EmuRogueSettings.Values.UseAdrenalineRush = (int)adrenlineRushNu.Value;
            EmuRogueSettings.Values.UseBladeFlurry = (int)bladeFlurryNu.Value;
            EmuRogueSettings.Values.UseEvasion = (int)evasionNu.Value;
            EmuRogueSettings.Values.UseBloodFury = (int)bloodFuryNu.Value;

            //Other Options
            EmuRogueSettings.Values.AlwaysStealthed = stealthCb.Checked;
            EmuRogueSettings.Values.UseKick = kickCb.Checked;
            EmuRogueSettings.Values.UseSprint = sprintCb.Checked;
            EmuRogueSettings.Values.MinimumSprintDistance = (int)sprintNu.Value;
            EmuRogueSettings.Values.MeleeAttackRange = (int) meleeRangeNu.Value;
            EmuRogueSettings.Values.RangedAttackRange = (int) rangeRangeNu.Value;

            //Misc
            EmuRogueSettings.Values.FoodName = foodTb.Text;

            EmuRogueSettings.Values.Save();
        }

        private void LoadFields()
        {
            EmuRogueSettings.Values.Load();

            //Ranged
            useRangedCb.Checked = EmuRogueSettings.Values.UseRangedToPull;
            rangedSpellTb.Text = EmuRogueSettings.Values.RangedPullSpell;
            rangedAmmoTb.Text = EmuRogueSettings.Values.RangedAmmo;

            //Damage Cooldowns
            adrenlineRushNu.Value = EmuRogueSettings.Values.UseAdrenalineRush;
            bladeFlurryNu.Value = EmuRogueSettings.Values.UseBladeFlurry;
            evasionNu.Value = EmuRogueSettings.Values.UseEvasion;
            bloodFuryNu.Value = EmuRogueSettings.Values.UseBloodFury;

            //Other Options
            stealthCb.Checked = EmuRogueSettings.Values.AlwaysStealthed;
            kickCb.Checked = EmuRogueSettings.Values.UseKick;
            sprintCb.Checked = EmuRogueSettings.Values.UseSprint;
            sprintNu.Value = EmuRogueSettings.Values.MinimumSprintDistance;
            meleeRangeNu.Value = EmuRogueSettings.Values.MeleeAttackRange;
            rangeRangeNu.Value = EmuRogueSettings.Values.RangedAttackRange;

            //Misc
            foodTb.Text = EmuRogueSettings.Values.FoodName;
        }

        private void reloadBtn_Click(object sender, EventArgs e)
        {
            LoadFields();
        }

        private void EmuRogueMain_Load(object sender, EventArgs e)
        {
            LoadFields();
        }
    }
}
