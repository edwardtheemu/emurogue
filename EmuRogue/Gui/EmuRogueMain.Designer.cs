﻿namespace EmuRogue.Gui
{
    partial class EmuRogueMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.rangedAmmoTb = new System.Windows.Forms.TextBox();
            this.rangedSpellTb = new System.Windows.Forms.TextBox();
            this.useRangedCb = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.rangeRangeNu = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.meleeRangeNu = new System.Windows.Forms.NumericUpDown();
            this.stealthCb = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.sprintNu = new System.Windows.Forms.NumericUpDown();
            this.sprintCb = new System.Windows.Forms.CheckBox();
            this.kickCb = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.bloodFuryNu = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.evasionNu = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.bladeFlurryNu = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.adrLbl = new System.Windows.Forms.Label();
            this.adrenlineRushNu = new System.Windows.Forms.NumericUpDown();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.foodTb = new System.Windows.Forms.TextBox();
            this.saveBtn = new System.Windows.Forms.Button();
            this.reloadBtn = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rangeRangeNu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meleeRangeNu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprintNu)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bloodFuryNu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.evasionNu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bladeFlurryNu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adrenlineRushNu)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(0, 1);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(385, 335);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(377, 309);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Combat";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.rangedAmmoTb);
            this.groupBox3.Controls.Add(this.rangedSpellTb);
            this.groupBox3.Controls.Add(this.useRangedCb);
            this.groupBox3.Location = new System.Drawing.Point(8, 15);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(190, 120);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Pulling";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 72);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Ranged Ammo";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Ranged Spell";
            // 
            // rangedAmmoTb
            // 
            this.rangedAmmoTb.Location = new System.Drawing.Point(83, 68);
            this.rangedAmmoTb.Name = "rangedAmmoTb";
            this.rangedAmmoTb.Size = new System.Drawing.Size(100, 20);
            this.rangedAmmoTb.TabIndex = 2;
            // 
            // rangedSpellTb
            // 
            this.rangedSpellTb.Location = new System.Drawing.Point(83, 42);
            this.rangedSpellTb.Name = "rangedSpellTb";
            this.rangedSpellTb.Size = new System.Drawing.Size(100, 20);
            this.rangedSpellTb.TabIndex = 1;
            // 
            // useRangedCb
            // 
            this.useRangedCb.AutoSize = true;
            this.useRangedCb.Location = new System.Drawing.Point(9, 19);
            this.useRangedCb.Name = "useRangedCb";
            this.useRangedCb.Size = new System.Drawing.Size(109, 17);
            this.useRangedCb.TabIndex = 0;
            this.useRangedCb.Text = "Pull With Ranged";
            this.useRangedCb.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.rangeRangeNu);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.meleeRangeNu);
            this.groupBox2.Controls.Add(this.stealthCb);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.sprintNu);
            this.groupBox2.Controls.Add(this.sprintCb);
            this.groupBox2.Controls.Add(this.kickCb);
            this.groupBox2.Location = new System.Drawing.Point(213, 15);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(153, 172);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Other Options";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 145);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Range Distance";
            // 
            // rangeRangeNu
            // 
            this.rangeRangeNu.Location = new System.Drawing.Point(100, 143);
            this.rangeRangeNu.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.rangeRangeNu.Minimum = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.rangeRangeNu.Name = "rangeRangeNu";
            this.rangeRangeNu.Size = new System.Drawing.Size(37, 20);
            this.rangeRangeNu.TabIndex = 16;
            this.rangeRangeNu.Tag = "";
            this.rangeRangeNu.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 119);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Melee Distance";
            // 
            // meleeRangeNu
            // 
            this.meleeRangeNu.Location = new System.Drawing.Point(100, 117);
            this.meleeRangeNu.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.meleeRangeNu.Name = "meleeRangeNu";
            this.meleeRangeNu.Size = new System.Drawing.Size(37, 20);
            this.meleeRangeNu.TabIndex = 14;
            this.meleeRangeNu.Tag = "";
            // 
            // stealthCb
            // 
            this.stealthCb.AutoSize = true;
            this.stealthCb.Location = new System.Drawing.Point(16, 22);
            this.stealthCb.Name = "stealthCb";
            this.stealthCb.Size = new System.Drawing.Size(95, 17);
            this.stealthCb.TabIndex = 13;
            this.stealthCb.Text = "Always Stealth";
            this.stealthCb.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 93);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Sprint Distance";
            // 
            // sprintNu
            // 
            this.sprintNu.Location = new System.Drawing.Point(100, 91);
            this.sprintNu.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.sprintNu.Name = "sprintNu";
            this.sprintNu.Size = new System.Drawing.Size(37, 20);
            this.sprintNu.TabIndex = 11;
            this.sprintNu.Tag = "";
            this.sprintNu.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // sprintCb
            // 
            this.sprintCb.AutoSize = true;
            this.sprintCb.Location = new System.Drawing.Point(16, 68);
            this.sprintCb.Name = "sprintCb";
            this.sprintCb.Size = new System.Drawing.Size(75, 17);
            this.sprintCb.TabIndex = 1;
            this.sprintCb.Text = "Use Sprint";
            this.sprintCb.UseVisualStyleBackColor = true;
            // 
            // kickCb
            // 
            this.kickCb.AutoSize = true;
            this.kickCb.Location = new System.Drawing.Point(16, 45);
            this.kickCb.Name = "kickCb";
            this.kickCb.Size = new System.Drawing.Size(69, 17);
            this.kickCb.TabIndex = 0;
            this.kickCb.Text = "Use Kick";
            this.kickCb.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.bloodFuryNu);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.evasionNu);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.bladeFlurryNu);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.adrLbl);
            this.groupBox1.Controls.Add(this.adrenlineRushNu);
            this.groupBox1.Location = new System.Drawing.Point(8, 141);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(190, 163);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Damage Cooldowns";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Use Blood Fury";
            // 
            // bloodFuryNu
            // 
            this.bloodFuryNu.Location = new System.Drawing.Point(135, 134);
            this.bloodFuryNu.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.bloodFuryNu.Name = "bloodFuryNu";
            this.bloodFuryNu.Size = new System.Drawing.Size(37, 20);
            this.bloodFuryNu.TabIndex = 9;
            this.bloodFuryNu.Tag = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 106);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Use Evasion";
            // 
            // evasionNu
            // 
            this.evasionNu.Location = new System.Drawing.Point(135, 104);
            this.evasionNu.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.evasionNu.Name = "evasionNu";
            this.evasionNu.Size = new System.Drawing.Size(37, 20);
            this.evasionNu.TabIndex = 7;
            this.evasionNu.Tag = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Use Blade Flurry";
            // 
            // bladeFlurryNu
            // 
            this.bladeFlurryNu.Location = new System.Drawing.Point(135, 74);
            this.bladeFlurryNu.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.bladeFlurryNu.Name = "bladeFlurryNu";
            this.bladeFlurryNu.Size = new System.Drawing.Size(37, 20);
            this.bladeFlurryNu.TabIndex = 5;
            this.bladeFlurryNu.Tag = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(32, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Num of adds - 0 to disable";
            // 
            // adrLbl
            // 
            this.adrLbl.AutoSize = true;
            this.adrLbl.Location = new System.Drawing.Point(16, 46);
            this.adrLbl.Name = "adrLbl";
            this.adrLbl.Size = new System.Drawing.Size(101, 13);
            this.adrLbl.TabIndex = 2;
            this.adrLbl.Text = "Use Adrenline Rush";
            this.adrLbl.Click += new System.EventHandler(this.adrLbl_Click);
            // 
            // adrenlineRushNu
            // 
            this.adrenlineRushNu.Location = new System.Drawing.Point(135, 44);
            this.adrenlineRushNu.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.adrenlineRushNu.Name = "adrenlineRushNu";
            this.adrenlineRushNu.Size = new System.Drawing.Size(37, 20);
            this.adrenlineRushNu.TabIndex = 1;
            this.adrenlineRushNu.Tag = "";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(377, 309);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Misc";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.foodTb);
            this.groupBox4.Location = new System.Drawing.Point(8, 16);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(200, 58);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Food and Potions";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 13);
            this.label10.TabIndex = 5;
            this.label10.Text = "Food Name";
            // 
            // foodTb
            // 
            this.foodTb.Location = new System.Drawing.Point(83, 22);
            this.foodTb.Name = "foodTb";
            this.foodTb.Size = new System.Drawing.Size(100, 20);
            this.foodTb.TabIndex = 4;
            // 
            // saveBtn
            // 
            this.saveBtn.Location = new System.Drawing.Point(41, 342);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(133, 28);
            this.saveBtn.TabIndex = 1;
            this.saveBtn.Text = "Save";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // reloadBtn
            // 
            this.reloadBtn.Location = new System.Drawing.Point(204, 342);
            this.reloadBtn.Name = "reloadBtn";
            this.reloadBtn.Size = new System.Drawing.Size(133, 28);
            this.reloadBtn.TabIndex = 2;
            this.reloadBtn.Text = "Reload";
            this.reloadBtn.UseVisualStyleBackColor = true;
            this.reloadBtn.Click += new System.EventHandler(this.reloadBtn_Click);
            // 
            // EmuRogueMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 381);
            this.Controls.Add(this.reloadBtn);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.tabControl1);
            this.Name = "EmuRogueMain";
            this.Text = "Emu Rogue";
            this.Load += new System.EventHandler(this.EmuRogueMain_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rangeRangeNu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meleeRangeNu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprintNu)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bloodFuryNu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.evasionNu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bladeFlurryNu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.adrenlineRushNu)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown evasionNu;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown bladeFlurryNu;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label adrLbl;
        private System.Windows.Forms.NumericUpDown adrenlineRushNu;
        private System.Windows.Forms.CheckBox kickCb;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown bloodFuryNu;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown sprintNu;
        private System.Windows.Forms.CheckBox sprintCb;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox rangedAmmoTb;
        private System.Windows.Forms.TextBox rangedSpellTb;
        private System.Windows.Forms.CheckBox useRangedCb;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown rangeRangeNu;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown meleeRangeNu;
        private System.Windows.Forms.CheckBox stealthCb;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox foodTb;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Button reloadBtn;
    }
}